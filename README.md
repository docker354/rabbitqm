# Comandos docker

### Detener contenedores

`docker stop $(docker ps -a -q)`

### Eliminar contenedores

`docker rm $(docker ps -a -q)`

### Eliminar imágenes

`docker rmi -f $(docker images -q)`

### Ejecutar docker-compose

`docker-compose up -d`
